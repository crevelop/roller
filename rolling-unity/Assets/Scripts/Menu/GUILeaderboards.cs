﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUILeaderboards : MonoBehaviour
{
    [SerializeField]
    UILabel first = null;

    [SerializeField]
    UILabel second = null;

    [SerializeField]
    UILabel third = null;

    [SerializeField]
    UIButton back = null;

    void OnEnable()
    {
        List<int> scores = GameData.Instance.TopScores;
        first.text = scores[0].ToString();
        second.text = scores[1].ToString();
        third.text = scores[2].ToString();
    }

    void Start()
    {
        back.onClick.Clear();
        back.onClick.Add(new EventDelegate(() =>
        {
            GameData.Instance.MenuState.Value = GUIMenu.Main;
        }));
    }
}
