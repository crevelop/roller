﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    [SerializeField]
    GameObject guiMain = null;

    [SerializeField]
    GameObject guiLeaderboards = null;

    void Start()
    {
        GameData.Instance.LoadFirstLevel();

        GameData.Instance.MenuState.AddEventAndFire(current =>
        {
            OnMenuChanged(current);
        }, this);
    }

    void OnMenuChanged(GUIMenu state)
    {
        SetPage(guiMain, state == GUIMenu.Main);
        SetPage(guiLeaderboards, state == GUIMenu.Leaderboards);
    }

    void SetPage(GameObject page, bool enabled)
    {
        if (page == null)
            return;
        page.SetActive(enabled);
    }

}

public enum GUIMenu
{
    Main,
    Leaderboards
}
