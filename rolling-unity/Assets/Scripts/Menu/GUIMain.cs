﻿using UnityEngine;
using System.Collections;

public class GUIMain : MonoBehaviour
{
    [SerializeField]
    UIButton play = null;

    [SerializeField]
    UIButton scores = null;

    void OnEnable()
    {

    }

    void Start()
    {
        play.onClick.Clear();
        play.onClick.Add(new EventDelegate(() =>
        {
            MatchData.Instance.State.Value = MatchState.PreGame;
            Application.LoadLevel("game");
        }));

        scores.onClick.Clear();
        scores.onClick.Add(new EventDelegate(() =>
        {
            GameData.Instance.MenuState.Value = GUIMenu.Leaderboards;
        }));
    }
}
