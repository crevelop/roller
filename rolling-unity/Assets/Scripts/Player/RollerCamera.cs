﻿using UnityEngine;
using System.Collections;

public class RollerCamera : MonoBehaviour 
{
    public Roller roller;
    public Transform CachedTransform { get; private set; }

    Vector3 offset;

    void Start()
    {
        CachedTransform = transform;
        offset = transform.position - roller.transform.position;
    }

    void LateUpdate()
    {
        CachedTransform.position = roller.CachedTransform.position + offset;
    }
}
