﻿using UnityEngine;
using System.Collections;

public class Roller : MonoBehaviour
{
	AudioSource audioSource = null;
    Rigidbody sphereBody = null;
    public Transform CachedTransform { get; private set; }

    float speed;

    void Start()
    {
        CachedTransform = transform;
        sphereBody = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource>();
        speed = MatchData.Instance.RollerSpeed;
    }

    void FixedUpdate()
    {
		float velocity = 0;

        if (MatchData.Instance.State.Value == MatchState.Running)
		{
            sphereBody.AddForce(new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")) * speed);
			velocity = sphereBody.velocity.magnitude;
		}

		if(audioSource)
		{
			if(!audioSource.isPlaying)
				audioSource.Play();
			audioSource.volume = Mathf.Clamp01(velocity/5f);
			audioSource.pitch = Mathf.Lerp(0.3f, 1.5f, velocity/12f);
		}
    }
}