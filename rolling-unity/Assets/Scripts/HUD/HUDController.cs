﻿using UnityEngine;
using System.Collections;

public class HUDController : MonoBehaviour 
{
    [SerializeField]
    GameObject preGameHUD = null;

    [SerializeField]
    GameObject runningGameHUD = null;

    [SerializeField]
    GameObject endOfGameHUD = null;

    void Start()
    {
        MatchData.Instance.State.AddEventAndFire(el =>
        {
            preGameHUD.SetActive(el == MatchState.PreGame);
            runningGameHUD.SetActive(el == MatchState.Running);
            endOfGameHUD.SetActive(el == MatchState.End);
        }, this);
    }
	
}
