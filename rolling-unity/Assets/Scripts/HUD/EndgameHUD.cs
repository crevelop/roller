﻿using UnityEngine;
using System.Collections;

public class EndgameHUD : MonoBehaviour 
{
    [SerializeField]
    UIButton play = null;

    [SerializeField]
    UIButton menu = null;

    [SerializeField]
    UILabel title = null;

    [SerializeField]
    UILabel message = null;

    [SerializeField]
    UILabel score = null;

    [SerializeField]
    UILabel timeBonus = null;

    [SerializeField]
    UILabel totalLevel = null;

    [SerializeField]
    UILabel totalScore = null;

	[SerializeField]
	AudioSource evilLaugh = null;

    void OnEnable()
    {
        UpdateResults();
    }

	void Start () 
    {
        play.onClick.Clear();
        play.onClick.Add(new EventDelegate(() =>
        {
            MatchData.Instance.ClearLevel();
            GameData.Instance.LoadNextLevel();
        }));

        menu.onClick.Clear();
        menu.onClick.Add(new EventDelegate(() =>
        {
            MatchData.Instance.Score.Value = 0;
            Application.LoadLevel("menu");
        }));
	}

    void UpdateResults()
    {
        int levelScore = MatchData.Instance.Score.Value;
        bool won = MatchData.Instance.HasWon;
        int totalAccumulated = MatchData.Instance.TotalScore;
        title.text = won ? "RIGHT... YOU WIN!" : "YOU LOSE";
        title.color = won ? Color.green : Color.red;
        message.text = won ? "Probably you were just lucky this time" : "Too bad, you don't belong to this realm anyway";

		score.text = string.Format("{0} pts",(levelScore - MatchData.Instance.TimeBonus));
		timeBonus.text = string.Format("{0} pts", MatchData.Instance.TimeLeft.Value);
		totalLevel.text = string.Format("{0} pts",levelScore);
		totalScore.text = string.Format("{0} pts", totalAccumulated);

        play.gameObject.SetActive(MatchData.Instance.HasNextLevel && won);

		if(!won)
			evilLaugh.Play();

		if(!MatchData.Instance.HasNextLevel && won)
			message.text = "Bah!...beaten by a human, who are you really?";

        GameData.Instance.CheckTopScore(totalAccumulated);
    }

   
	
}
