﻿using UnityEngine;
using System.Collections;

public class PregameHUD : MonoBehaviour {

    [SerializeField]
    UIButton startButton = null;

    [SerializeField]
    UILabel title = null;

    [SerializeField]
    UILabel message = null;

    [SerializeField]
    UILabel objective = null;

    void OnEnable()
    {
        SetValues(MatchData.Instance.Level);
    }

	void Start () 
    {
        startButton.onClick.Clear();
        startButton.onClick.Add(new EventDelegate(() =>
            {
                MatchData.Instance.State.Value = MatchState.Running;
            }));
	}

    void SetValues(CardLevel level)
    {
        title.text = string.Format("LEVEL {0}", level.Index);
        message.text = level.WelcomeMessage;
        string cubes = GUIUtils.ColorToNGuiModifier(Color.yellow) + MatchData.Instance.Enemies.Count + "[-]";
        string time = GUIUtils.ColorToNGuiModifier(Color.yellow) + level.TimeLimit + "[-]";
        objective.text = string.Format("Try hitting {0} Cubes in {1} seconds :D", cubes, time);
    }
}
