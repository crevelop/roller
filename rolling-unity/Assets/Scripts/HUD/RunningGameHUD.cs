﻿using UnityEngine;
using System.Collections;

public class RunningGameHUD : MonoBehaviour
{
    [SerializeField]
    UILabel hits = null;

    [SerializeField]
    UILabel score = null;

    [SerializeField]
    UILabel timer = null;

    [SerializeField]
    UILabel streak = null;

	[SerializeField]
	UIProgressBar hitsBar = null;

	[SerializeField]
	UIProgressBar timeBar = null;

	[SerializeField]
	UILabel pointsLabel = null;

    void OnEnable()
    {
        MatchData.Instance.Hits.Fire(this);
        MatchData.Instance.Score.Fire(this);
		pointsLabel.gameObject.SetActive(false);
    }

    void Start()
    {
        MatchData.Instance.Hits.AddEventAndFire(el =>
        {
            hits.text = string.Format("{0}/{1}", GUIUtils.ColorToNGuiModifier(Color.yellow) + el.ToString() + "[-]", MatchData.Instance.Goal);

            int currentStreak = MatchData.Instance.Streak;
            CardEnemy currentEnemy = MatchData.Instance.CurrentHit;

			hitsBar.value = Mathf.Clamp01((float)el/MatchData.Instance.Goal);

            streak.gameObject.SetActive(currentStreak > 1);
            if (currentEnemy != null)
                streak.text = string.Format("x{0}", GUIUtils.ColorToNGuiModifier(currentEnemy.GetColor) + (currentStreak +1).ToString() + "[-]");
        }, this);

        MatchData.Instance.Score.AddEventAndFire(el =>
        {
            score.text = string.Format("{0} pts", el.ToString());
        }, this);

		MatchData.Instance.CurrentHit.AddEventAndFire(el =>
		{
			if(el!=null)
				StartCoroutine(ShowPoints(el.Points, MatchData.Instance.Streak));
			else
				pointsLabel.gameObject.SetActive(false);
		}, this);

        MatchData.Instance.TimeLeft.AddEventAndFire(el =>
        {
            timer.text = GUIUtils.GetClockString(el);

			timeBar.value = Mathf.Clamp01((float)el/MatchData.Instance.TimeLimit);

			timeBar.foregroundWidget.color = new Color(1-timeBar.value, timeBar.value, 0f, 1f);
        }, this);
    }

	IEnumerator ShowPoints(int points, int streak)
	{
		pointsLabel.gameObject.SetActive(true);
		pointsLabel.text = string.Format("+{0} {1}", points, streak >1? "x"+streak.ToString() : "");

		yield return new WaitForSeconds(2f);

		pointsLabel.gameObject.SetActive(false);
	}
}