﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : MonoSingleton<GameData>
{
    public Dictionary<int, CardEnemy> EnemyTypes = new Dictionary<int, CardEnemy>();

    public List<CardLevel> Levels = new List<CardLevel>();

    public Property<GUIMenu> MenuState = new Property<GUIMenu>(GUIMenu.Main);

    public List<int> TopScores
    {
        get
        {
            List<int> scores = new List<int>();

            for (int i = 1; i < 4; i++)
            {
                scores.Add(PlayerPrefs.GetInt("top_score_" + i, 0));
            }

            return scores;
        }
    }

    protected override void Initialize()
    {
        Load();
        if (Application.loadedLevelName == "game")
        {
            Debug.Log("Running on Debug Mode, no worries you can still play, just to experience full game loop try playing from menu scene");
            LoadFirstLevel();
        }
    }

    [ContextMenu("Load Cards")]
    public void Load()
    {
        Debug.Log("Loading CardGame from Resources");
        var asset = Resources.Load<TextAsset>("CardGame");

        if (asset == null || string.IsNullOrEmpty(asset.text))
        {
            Debug.LogError("No CardGame Json found in Resources");
            return;
        }

        Debug.Log("CardGame JSON: " + asset.text);

        var gameCard = JsonHelper.Deserialize<CardGame>(asset.text);

        EnemyTypes.Clear();
        Levels.Clear();

        foreach (CardEnemy e in gameCard.EnemyTypes)
            EnemyTypes.Add(e.Id, e);

        foreach (CardLevel l in gameCard.Levels)
            Levels.Add(l);

        Levels.Sort((el1, el2) => el1.Index.CompareTo(el2.Index));

        Debug.Log(EnemyTypes.Count.ToString() + " Enemy types Loaded");
        Debug.Log(Levels.Count.ToString() + " Levels Loaded");
    }

    public void LoadFirstLevel()
    {
        MatchData.Instance.ClearProgress();
        if (Levels.Count > 0)
            MatchData.Instance.LoadLevel(Levels[0]);
    }

    public void LoadNextLevel()
    {
        var next = GetNextLevel(MatchData.Instance.Level);
        if (next != null)
            MatchData.Instance.LoadLevel(next);
        else
            Debug.LogError("No more levels found");
    }

    public CardLevel GetNextLevel(CardLevel currentLevel)
    {
        if (currentLevel == null && Levels.Count > 0)
            return Levels[0];

        var current = Levels.Find(el => el.Id == currentLevel.Id);
        CardLevel next = null;
        if (current != null)
        {
            int currentIndex = Levels.IndexOf(current);
            next = (currentIndex + 1 < Levels.Count) ? Levels[currentIndex + 1] : null;
        }
        return next;
    }

    public void CheckTopScore(int newScore)
    {
        List<int> temp = TopScores;
        temp.Add(newScore);
        temp.Sort((s1,s2) => s2.CompareTo(s1));
        temp.RemoveAt(temp.Count - 1);

        int i = 1;
        temp.ForEach(score =>
            {
                PlayerPrefs.SetInt("top_score_" + i, score);
                i++;
            });
    }
}
