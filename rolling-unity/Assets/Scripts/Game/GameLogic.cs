﻿using UnityEngine;
using System.Collections;

public class GameLogic : MonoBehaviour 
{
    [SerializeField]
    Collectable enemyPrefab = null;

    float elapseTime = 0;

	void Start () 
    {
        MatchData.Instance.Hits.AddEventAndFire(el =>
            {
                if (MatchData.Instance.State.Value == MatchState.Running && el >= MatchData.Instance.Goal)
                    FinishGame();
            }, this);

        MatchData.Instance.CurrentHit.AddEvent(el =>
            {
                if (el != null)
                {
                    CardEnemy previous = MatchData.Instance.PreviousHit;
                    if (previous != null && previous.CubeByteColor == el.CubeByteColor)
                        MatchData.Instance.Streak++;
                    else
                        MatchData.Instance.Streak = 1;

                    MatchData.Instance.Hits.Value++;
                    MatchData.Instance.Score.Value += el.Points * MatchData.Instance.Streak;
                    MatchData.Instance.PreviousHit = el;
                }
            }, this);

        MatchData.Instance.State.AddEventAndFire(el =>
            {
                if (el == MatchState.Running)
                    SpawnEnemies();
            }, this);
	}

    void SpawnEnemies()
    {
        var enemies = MatchData.Instance.Enemies;
        var radius = MatchData.Instance.SpawnRadius;
        foreach (CardEnemy e in enemies)
        {
            GameObject go = (GameObject) GameObject.Instantiate(enemyPrefab.gameObject);
            go.transform.parent = transform;
            go.transform.position = new Vector3(Random.Range(-radius, radius), 2f, Random.Range(-radius, radius));
            Collectable c = go.GetComponent<Collectable>();
            c.Set(e);
        }
    }

    void FinishGame()
    {
        foreach (Transform t in transform)
            Destroy(t.gameObject);
        MatchData.Instance.TimeBonus = MatchData.Instance.TimeLeft.Value;
        MatchData.Instance.Score.Value += MatchData.Instance.TimeBonus;
        MatchData.Instance.TotalScore += MatchData.Instance.Score.Value;

        elapseTime = 0;

        MatchData.Instance.State.Value = MatchState.End;
    }

    void Update()
    {
        if (MatchData.Instance.State.Value == MatchState.Running)
        {
            elapseTime += Time.deltaTime;
            int val = Mathf.CeilToInt(MatchData.Instance.TimeLimit - elapseTime);
            if (MatchData.Instance.TimeLeft == val) return;
            MatchData.Instance.TimeLeft.Value = val;

            if (val == 0)
                FinishGame();
        }
    }
}
