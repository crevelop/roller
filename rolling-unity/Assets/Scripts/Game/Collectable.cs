﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour
{
    [SerializeField]
    GameObject destroyFX = null;

    bool wasTouched = false;

    CardEnemy enemyData = null;

    public void Set(CardEnemy enemy)
    {
        enemyData = enemy;
        renderer.material.color = enemy.GetColor;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Roller" && !wasTouched)
        {
            wasTouched = true;
            StartCoroutine(Destruction());
        }
    }

    IEnumerator Destruction()
    {
        MatchData.Instance.CurrentHit.Value = enemyData;
        GameObject.Instantiate(destroyFX, transform.position, Quaternion.identity);

        yield return new WaitForEndOfFrame();
        gameObject.SetActive(false);
    }
}
