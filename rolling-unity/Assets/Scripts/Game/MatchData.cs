﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MatchData 
{
    public static MatchData Instance = new MatchData();

    public Property<int> TimeLeft = new Property<int>(0);
    public Property<int> Hits = new Property<int>(0);
    public Property<int> Score = new Property<int>(0);
    public Property<MatchState> State = new Property<MatchState>(MatchState.PreGame);

    public Property<CardEnemy> CurrentHit = new Property<CardEnemy>(null);
    public CardEnemy PreviousHit = null;

    public bool HasWon { get { return Hits.Value >= Goal; } }

    public int LevelIndex = -1;
    public int Streak = 1;
    public int TimeLimit = 60;
    public float SpawnRadius = 20f;
    public float RollerSpeed = 100f;
    public int TotalScore = 0;
    public int TimeBonus = 0;

    public string WelcomeMessage = "";

    public int Goal { get { return Enemies.Count;  } }

    public CardLevel Level = null;
    public List<CardEnemy> Enemies = new List<CardEnemy>();

    public bool HasNextLevel { get { return GameData.Instance.GetNextLevel(Level) != null; } }

    public void LoadLevel(CardLevel level)
    {
        Level = level;
        Enemies.Clear();

        if (Level != null)
        {
            
            var enemyTypes = GameData.Instance.EnemyTypes;
            TimeLimit = Level.TimeLimit;
            WelcomeMessage = Level.WelcomeMessage;

            foreach (CardLevelEnemy lEnemy in Level.Enemies)
            {
                if (enemyTypes.ContainsKey(lEnemy.EnemyId))
                {
                    for (int i = 0; i < lEnemy.Quantity; i++)
                        Enemies.Add(enemyTypes[lEnemy.EnemyId]);
                }
                else
                    Debug.LogError("Could not find EnemyType with id: " + lEnemy.EnemyId.ToString());
            }
            Debug.Log("Level: " + Level.Id.ToString()+" loaded and ready :)");

            State.Value = MatchState.PreGame;
        }
        else
            Debug.LogError("Can't find next level");
    }

    public void ClearProgress()
    {
        ClearLevel();
        TotalScore = 0;
    }

    public void ClearLevel()
    {
        Score.Value = 0;
        Hits.Value = 0;
        PreviousHit = null;
        CurrentHit.Value = null;
        Streak = 1;
    }
}

public enum MatchState
{
    PreGame,
    Running,
    End
}
