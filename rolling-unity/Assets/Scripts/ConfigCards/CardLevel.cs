﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardLevel
{
    [JsonDeserialize("id", true)]
    public int Id = 0;

    [JsonDeserialize("index")]
    public int Index = 0;

    [JsonDeserialize("time_limit")]
    public int TimeLimit = 0;

    [JsonDeserialize("welcome_message")]
    public string WelcomeMessage = "";

    [JsonDeserialize("enemies")]
    public List<CardLevelEnemy> Enemies = new List<CardLevelEnemy>();
}