﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardGame
{
    [JsonDeserialize("enemy_types")]
    public List<CardEnemy> EnemyTypes = new List<CardEnemy>();

    [JsonDeserialize("levels")]
    public List<CardLevel> Levels = new List<CardLevel>();
}