﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardEnemy 
{
    [JsonDeserialize("id", true)]
    public int Id = 0;

    [JsonDeserialize("points")]
    public int Points = 0;

    [JsonDeserialize("cube_color")]
    public List<byte> CubeByteColor = new List<byte>();

    public Color GetColor
    {
        get
        {
            return CubeByteColor.Count > 0 ? GUIUtils.GetColorFromBytes(CubeByteColor) : Color.white;
        }
    }
}