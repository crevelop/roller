﻿using UnityEngine;
using System.Collections;

public class CardLevelEnemy
{
    [JsonDeserialize("id")]
    public int EnemyId = 0;

    [JsonDeserialize("quantity")]
    public int Quantity = 0;
}
