﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GUIUtils {

    public static string ColorToNGuiModifier(Color color)
    {
        int r = (int)(255 * color.r);
        int g = (int)(255 * color.g);
        int b = (int)(255 * color.b);

        return "[" + r.ToString("X2") + g.ToString("X2") + b.ToString("X2") + "]";
    }

    public static string GetClockString(int remainingSeconds)
    {
        int hours = remainingSeconds / 3600;
        int minutes = (remainingSeconds % 3600) / 60;
        int seconds = remainingSeconds % 60;

        if (hours > 0)
            return string.Format("{0}:{1}:{2}", hours.ToString(), minutes.ToString("00"), seconds.ToString("00"));
        else
            return string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));
    }

    public static Color GetColorFromBytes(List<byte> color)
    {
        return new Color((float)color[0] / 255f, (float)color[1] / 255f, (float)color[2] / 255f, (float)color[3] / 255f);
    }
}
